package com.example.appclima.view.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appclima.R
import kotlinx.android.synthetic.main.activity_information.*

class InformationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_information)
        
        getDataWeather()
        
        
    }
    
    private fun getDataWeather(){

        val bundle =  intent.extras

        val cityName = bundle?.getString("name")
        val description = bundle?.getString("description")
        val grados = bundle?.getDouble("grado")

        drawInfo(cityName,description,grados)
        
    }

    @SuppressLint("SetTextI18n")
    private fun drawInfo(cityName: String?, description: String?, grados: Double?) {

        if (grados != null) {
            if(grados>20.0){
                imageViewClima.setImageResource(R.drawable.cloud)
            }else{
                imageViewClima.setImageResource(R.drawable.umbrella)
            }
        }

        textViewCity.text = cityName
        textViewDescrip.text = description
        textViewGrados.text = "${grados}°"

    }
    
}
