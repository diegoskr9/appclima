package com.example.appclima.view.util

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

open class MyApp : AppCompatActivity() {

    fun showToast(message:Int){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }

}