package com.example.appclima.view.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.appclima.R
import com.example.appclima.view.model.City
import com.example.appclima.view.util.MyApp
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : MyApp() {

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.AppTheme)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



    }

    private fun getData(myCity:String){

        //4f55508ad37c2f3b2fca3a115784a320

        val queue = Volley.newRequestQueue(this)

        val url = "http://api.openweathermap.org/data/2.5/weather?q=${myCity}&appid=4f55508ad37c2f3b2fca3a115784a320&units=metric&lang=es"

        val request = JsonObjectRequest(Request.Method.GET,url,null, Response.Listener { response ->

            try {

                val city = Gson().fromJson(response.toString(),City::class.java)

                goView(city)

            }catch (e:Exception){


            }

        }, Response.ErrorListener {error ->

            //showToast(R.string.citynotfound)

            Toast.makeText(this,error.toString(),Toast.LENGTH_SHORT).show()

        })
        queue.add(request)

    }

    fun onClick(view: View){

        if(editTextCity.text.isNullOrEmpty()){
            showToast(R.string.cityempty)
        }else{
            getData(editTextCity.text.toString())

        }

    }

    private fun goView(city: City) {

        val intent = Intent(this,InformationActivity::class.java).apply {

            putExtra("name",city.name)
            putExtra("description", city.weather?.get(0)?.description)
            putExtra("grado",city.main?.temp)

        }
        startActivity(intent)


    }



}
